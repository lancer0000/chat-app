class rooms {
    constructor() {
        this.activeRooms = [];
    }

    addRoom(room) {
        this.activeRooms.push(room);
    }

    getRooms() {
        return [...this.activeRooms];
    }

    removeRoom(room) {
        let deletedRoom = this.activeRooms.find(rm => rm === room);
        this.activeRooms.splice(this.activeRooms.indexOf(deletedRoom), 1);
        return deletedRoom;
    }
}

module.exports = rooms;
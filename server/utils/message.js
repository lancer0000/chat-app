const moment = require('moment');

const generateMessage = (from, text) => {
    return {
        from: from,
        text: text,
        sentAt: moment.valueOf()
    };
};

module.exports = generateMessage;
const socket = io();
let roomsDropdown = jQuery('#rooms-dropdown');
let uniqueRooms = [];

socket.on('updateActiveRoomsList', (rooms) => {
    //remove all elements upon getting the event & the new updated rooms array
    roomsDropdown.find('option').not(':first').remove();
    // rooms.forEach(room => {
    //     if (roomsDropdown.find('option:visible').text() === room) {
    //         roomsDropdown.append(jQuery('<option></option>').text(room).hide());
    //         console.log(roomsDropdown);
    //         return;
    //     }
    //     roomsDropdown.append(jQuery('<option></option>').text(room));
    // });
    //second try
    // jQuery.each(rooms, (i, el) => {
    //     if(jQuery.inArray(el, uniqueRooms) === -1) uniqueRooms.push(el); 
    // });
    // uniqueRooms.forEach(room => {
    //     roomsDropdown.append(jQuery('<option></option>').text(room));
    // });
    rooms = rooms.filter((item, pos) => {
        return rooms.indexOf(item) == pos;
    });
    rooms.forEach(room => {
        roomsDropdown.append(jQuery('<option></option>').text(room));
    });
});

socket.on('disconnect', () => {
    console.log('Server disconnected!');
});
